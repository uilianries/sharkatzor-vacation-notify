import collections
import random
import requests
import logging
import argparse
import configparser


LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)
S_HANDLER = logging.StreamHandler()
S_HANDLER.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s [%(levelname)s] %(message)s')
S_HANDLER.setFormatter(formatter)
LOGGER.addHandler(S_HANDLER)


YoutubeVideo = collections.namedtuple("YoutubeVideo", ["id", "title"])

class RandomVideo(object):

    def __init__(self):
        # INFO: List captured on December 10th, 2023. Using most_watched_videos.py
        self._videos = [
            YoutubeVideo("dGec2k-Mx9E", "Coloquei a NOVA civilização contra uma IA no EXTREMO - Age of Empires 2"),
            YoutubeVideo("zWMtDAsbnO0", "ESSA PARTIDA FOI LENDÁRIA | Age of Empires 2"),
            YoutubeVideo("6HvVMHYd1Og", "NOVA EXPANSÃO DO AGE OF EMPIRES 2 EM 2023!"),
            YoutubeVideo("McYfZczHwHg", "O MELHOR mapa DO MUNDO voltou | Age of Empires 2"),
            YoutubeVideo("7I9cgbdYiYQ", "A CONQUISTA DA IBÉRIA - 2v2v2v2  | Age of Empires 2"),
            YoutubeVideo("dfWsMm9d7sc", "A tática DOENTE | Age of Empires 2"),
            YoutubeVideo("UtvKm0QCmbA", "NINGUÉM TAVA PURO NESSE JOGO | Age of Empires 2"),
            YoutubeVideo("majWM0ksgHA", "UM TIME COM 4 ROMANOS? | Age of Empires 2"),
            YoutubeVideo("0KKo2JSg-2I", "UM ENXAME DE TROPAS | Age of Empires 2"),
            YoutubeVideo("zoisK8Sd0uk", "AS VEZES NEM FOCADO DÁ | Age of Empires 2"),
            YoutubeVideo("DkKjvK2-ot4", "TRETAS E TRAIÇÕES NA FLORESTA SEM FIM - Floresta e mais nada 3ª Edição | AGE OF EMPIRES 2"),
            YoutubeVideo("1MjgGr2LMaA", "OS ETERNOS BASTIÕES DO MICHI | Age of Empires 2"),
            YoutubeVideo("ghztSvLKDLo", "PERRENGUE NO MICHI | Age of Empires 2"),
            YoutubeVideo("kpdk10ZyJUc", "FIASCO | Age of Empires 2"),
            YoutubeVideo("4aZIT1_ZTiY", "MURALHA DE ELEFANTES | Age of Empires 2"),
            YoutubeVideo("BryMqE85dQc", "FIZ UM COACH DE AGE OF EMPIRES ft. Fasturtle | Age of Empires 2"),
            YoutubeVideo("hHDWA1fctKM", "PIRATAS | Age of Empires 2"),
            YoutubeVideo("9PkOWABHrh8", "OS INCONTESTÁVEIS SOBERANOS DO MICHI  | Age of Empires 2"),
            YoutubeVideo("9J6kgFXaU8Y", "ISTO É MAIAS | Age of Empires 2"),
            YoutubeVideo("PkpfVtTD2s0", "O CAMPO FOI LAVADO (de sangue) | Age of Empires 2"),
            YoutubeVideo("8Oocq1es__Q", "O PODER DE FOGO ESPANHOL | Age of Empires 2"),
            YoutubeVideo("q-6Z2UAZ-k8", "UM HOMEM DE DICAS | Age of Empires 2"),
            YoutubeVideo("N-_-nuJNuHU", "OS REIS DO MICHI | Age of Empires 2"),
            YoutubeVideo("zf2IrmjuQ74", "QUANDO O PONTA TEM A MAIOR PONTUAÇÃO | Age of Empires 2"),
            YoutubeVideo("fX8T8FMaEWU", "CIVILIZAÇÕES NOMÂDICAS - ChampionSheep - LIGA TITÃ (Gabi x Paganini) Jogo 3 | Age of Empires 2"),
            YoutubeVideo("YyoUSHUBuVU", "NUNCA COMECE UM JOGO COM ESSA MENTALIDE! | Age of Empires 2"),
            YoutubeVideo("K_Os8c8UuK0", "O velho clássico! - Brasil x Argentina - Jogo 1 | Age of Empires 2"),
            YoutubeVideo("BCxsi9NYH-U", "FINALMENTE JOGAMOS AGE OF EMPIRES 3! | Age of Empires 3"),
            YoutubeVideo("1_1k5zwvNBo", "ABANDONADO PELO PRÓPRIO TIME | Age of Empires 2"),
            YoutubeVideo("t9hodDH_XU4", "SUB-ZERO, O PESO INDESTRUTÍVEL | Age of Empires 2"),
            YoutubeVideo("ZFGFNQYYsl8", "ALTAS EMOÇÕES - ChampionSheep - LIGA TITÃ (Pedreiro x Alive) Jogo 2 | Age of Empires 2"),
            YoutubeVideo("kwLyAy6tG48", "É O BRASIL, NÃO TEM JEITO - ChampionSheep - LIGA TITÃ (Dogão x Capoch) Jogo 4 | Age of Empires 2"),
            YoutubeVideo("LpQ9A-SrgSc", "O FAST IMP MAIS RÁPIDO DO VELHO-OESTE | Age of Empires 2"),
            YoutubeVideo("wvBXrdztexw", "OS TITÃS BRASILEIROS - ChampionSheep - LIGA TITÃ (Gabi x Paganini) Jogo 1 | Age of Empires 2"),
            YoutubeVideo("BTC1xq13SKE", "Georgianos vs IA EXTREMA, será que é boa? - Age of Empires 2"),
            YoutubeVideo("43gN5sWUjmg", "A ESTRATRÉGIA PROIBIDA | Age of Empires 2"),
            YoutubeVideo("bVYzn6-Ds_0", "O PODER DE FOGO ESLAVO | Age of Empires 2"),
            YoutubeVideo("VjjnbYoMZGE", "LARGADOS E PELADOS | Age of Empires 2"),
            YoutubeVideo("5xmO89KrgxM", "Não tenho nem um título para esse jogo | Age of Empires 2"),
            YoutubeVideo("fxm1viwGWuc", "AGE 3 EM BREVE? | Age of Empires 2"),
            YoutubeVideo("TBvf_sP0VYM", "O DOGÃO TEM UM PLANO... - DOGÃO x MIGUEL - Jogo 2 | Age of Empires 2"),
            YoutubeVideo("WHamiEfJBfM", "CADÊ OS PALADINOS? | Age of Empires 2"),
            YoutubeVideo("gBALE6uh8GA", "UM PASSEIO NA FLORESTA | Age of Empires 2"),
            YoutubeVideo("JTjYYDHEIKs", "ME RESPONDAM ESSA PERGUNTA - ChampionSheep - LIGA TITÃ (Mehmed x chaelzzz) Jogo 3 | Age of Empires 2"),
            YoutubeVideo("w0XVLIjxQ6c", "ISSO AQUI EU NUNCA VI - ChampionSheep - LIGA TITÃ (Nahue x Dogão) Jogo 3 | Age of Empires 2"),
            YoutubeVideo("3qDNjCUjSn8", "COORDENAÇÃO É TUDO | Age of Empires 2"),
            YoutubeVideo("DxYBz6Bw9n0", "DEIXA TUDO NAS MÃOS DO CRASH | Age of Empires 2"),
            YoutubeVideo("IHHbWskzLLE", "AMANTES DE TORRES - ChampionSheep - LIGA OURO (Alone x Gen. Enk) Jogo 1 | Age of Empires 2"),
            YoutubeVideo("tejCUnzIzjU", "UMA JOGADA MUDA TUDO - ChampionSheep - LIGA TITÃ (st4rk x Dream) Jogo 3 | Age of Empires 2"),
            YoutubeVideo("08XIj-_WGj4", "DELETA O MURO POR FAVOR!! | Age of Empires 2"),
            YoutubeVideo("eOFolXvt4tM", "DUELO DE CAVALARIA - Dogao x st4rk - Jogo 6 | Age of Empires 2"),
            YoutubeVideo("MooePL2_Z5U", "A Grande Migração | Age of Empires 2"),
            YoutubeVideo("cnXS-ymwvW4", "Os clãfrontos voltaram! Bk x Ss | Age of Empires 2"),
            YoutubeVideo("o27t0_Y6Cog", "O QUE MAIS VEM DE NOVO NO AGE OF EMPIRES 2? - Atualização de Outubro 2023"),
            YoutubeVideo("yQy_D1LawW4", "AGORA TEM DOIS FAST? | Age of Empires 2"),
            YoutubeVideo("WDVev5ZtMa8", "A grande clareira! Bk x Ss | Age of Empires 2"),
            YoutubeVideo("6XfHYCde_G0", "A grande RESISTÊNCIA Eslava! | Age of Empires 2"),
            YoutubeVideo("9_ryXqgmO_w", "O MAIOR SHOWMATCH DO BRASIL! - DOGÃO x MIGUEL - Jogo 1 | Age of Empires 2"),
            YoutubeVideo("iiFz_criiD4", "ENTRARAM COMO? | Age of Empires 2"),
            YoutubeVideo("lElBC9eGRWg", "NUNCA DUVIDE DO PODER ESLAVO | Age of Empires 2"),
            YoutubeVideo("Bj0r3BUYvpk", "ABAIXA O VOLUME DA TV E ME ESCUTA PELO TELEFONE | Age of Empires 2"),
            YoutubeVideo("0jplObKgyu4", "COMEÇO DAS OITAVAS DE FINAL - ChampionSheep - LIGA TITÃ (Nahue x Dogão) Jogo 1 | Age of Empires 2"),
            YoutubeVideo("f_RHADWFEEU", "O PODER DA AMIZADE - Live de 24 Horas  | Age of Empires 2"),
            YoutubeVideo("Plkrm1PZ9y8", "BEM DIFÍCIL AMIGOS | Age of Empires 2"),
            YoutubeVideo("oRDtSZyETmo", "A melhor civilização do jogo - Pater x Felino Molhado - Jogo 2 | Age of Empires 2"),
            YoutubeVideo("aFZbKsM4dVE", "O TAL DO GODOS PONTA | Age of Empires 2"),
            YoutubeVideo("ob-VWsJb5jg", "DOMINAÇÃO TOTAL - DOGÃO x MIGUEL - Jogo 5 | Age of Empires 2"),
            YoutubeVideo("zzSb9ahd97k", "O MAPA DAS DECISÕES  - Brasil x Argentina - Jogo 2 | Age of Empires 2"),
            YoutubeVideo("yI4HtLQSGbs", "A GUERRA INACABÁVEL - Live de 24 Horas  | Age of Empires 2"),
            YoutubeVideo("RhHHKW8Otxw", "O RESGATE ESLAVO | Age of Empires 2"),
            YoutubeVideo("4MaoNaXSwi4", "HERMANOS (parte 1) | Age of Empires 2"),
            YoutubeVideo("NgujreHdDmM", "DESEMPATE! - Dogao x st4rk - Jogo 5 | Age of Empires 2"),
            YoutubeVideo("-3SYmodr_yk", "TIME SEM CÉREBRO | Age of Empires 2"),
            YoutubeVideo("DF68-NQZyQY", "DROMONS! - Follen x Caboclo - Jogo 2 | Age of Empires 2"),
            YoutubeVideo("sZUupJOLCiU", "MENSAGEM PARA OS SEGUIDORES | Age of Empires 2"),
            YoutubeVideo("y-nZFQPVNBI", "FALADOR PASSA MAL - ChampionSheep - LIGA OURO (vitinchan x Klecius) Jogo 1 | Age of Empires 2"),
            YoutubeVideo("VhUrQf4HasA", "COLOCANDO EM PRÁTICA O COACHING ft. Fasturtle | Age of Empires 2"),
            YoutubeVideo("QqZTWs6V0oc", "TÁTICAS DIFERENCIADAS - ChampionSheep - LIGA PRATA (Weslan x Xefres) Jogo 2 | Age of Empires 2"),
            YoutubeVideo("2qDfGJupnI0", "ALEGRIA NOS PÉS | Age of Empires 2"),
            YoutubeVideo("YbUBSyBGr8M", "OS VERDADEIROS REIS DA SELVA - Live de 24 Horas  | Age of Empires 2"),
            YoutubeVideo("DmsZvhxeDcM", "OS CARA SÃO NÔMADEIROS - TEAM GABI x TEAM BK - Jogo 2 | Age of Empires 2"),
            YoutubeVideo("Yx2hSYkhdYQ", "O PODER DA CIV CERTA! - ChampionSheep - LIGA TITÃ (Jubileu x Lucho) Jogo 3 | Age of Empires 2"),
            YoutubeVideo("wi8Li--UNEs", "DOMINAÇÃO DOS 7 MARES - ChampionSheep - LIGA TITÃ (Dogão x st4rk) Jogo 4 | Age of Empires 2"),
            YoutubeVideo("UH5E9j-tTgE", "QUE PO*** É ESSA? | Age of Empires 2"),
            YoutubeVideo("v3xDOJl7sU4", "ELE VAI ME LEVAR ONDE? | Age of Empires 2"),
            YoutubeVideo("8s1cL0mr368", "O JUBILEU ESTÁ ESTRANHO HOJE - ChampionSheep - LIGA TITÃ (Jubileu x Lucho) Jogo 2 | Age of Empires 2"),
            YoutubeVideo("XYXezvMR6zM", "DOGÃO, UMA AULA DE PORTUGUESES - DOGÃO x MIGUEL - Jogo 3 | Age of Empires 2"),
            YoutubeVideo("ESFSjUAZdsE", "UM MAPA HISTÓRICO - ChampionSheep - LIGA TITÃ (Gabi x Paganini) Jogo 2 | Age of Empires 2"),
            YoutubeVideo("pPcFUnyDRKE", "SERVENTE DESAFIOU O PATRÃO - Follen x Chefe do Fasturtle - Jogo 1 | Age of Empires 2"),
            YoutubeVideo("urrg7LINorE", "MAS OQUE QUE ACONTECEU? | Age of Empires 2"),
            YoutubeVideo("rM2_smLtaqQ", "QUE É ISSO DOG! - ChampionSheep - LIGA TITÃ (Dogão x Nicov) Jogo 5 | Age of Empires 2"),
            YoutubeVideo("TMxtNK8bAnI", "O CHAT FEZ UMA BRINCADEIRA DURANTE A FINAL... - TEAM GABI x TEAM BK - Jogo 5 | Age of Empires 2"),
            YoutubeVideo("A8XPrmVzdKM", "MEIA VITÓRIA | Age of Empires 2"),
            YoutubeVideo("vB36dvhtTrI", "APOSTANDO NO AGGRO - ChampionSheep - LIGA TITÃ (Dogão x Nicov) Jogo 3 | Age of Empires 2"),
            YoutubeVideo("inkuE2OvafE", "A saidera!! Bk x Ss | Age of Empires 2"),
            YoutubeVideo("biqty80t41Y", "RÁPIDO E AGRESSIVO - ChampionSheep - LIGA TITÃ (Dogão x Nicov) Jogo 4 | Age of Empires 2"),
            YoutubeVideo("j59YGtoYJE4", "EU QUERO UM MAPA ASSIM! - ChampionSheep - LIGA TITÃ (Pedreiro x Alive) Jogo 3 | Age of Empires 2"),
            YoutubeVideo("3ARq1CNM0R0", "A VERDADE SOBRE O AGE 4 | Age of Empires 2"),
            YoutubeVideo("wT6nVIgL390", "CORTE RÁPIDO! - ChampionSheep - LIGA TITÃ (Mehmed x chaelzzz) Jogo 2 | Age of Empires 2"),
            YoutubeVideo("8DSwizpdaeI", "ABANDONANDO A PESCA | Age of Empires 2"),
            YoutubeVideo("aj04PyjALoc", "O HOMEM DO PALPITE - ChampionSheep - LIGA TITÃ (Sebastian x Nicov) Jogo 5 | Age of Empires 2"),
            YoutubeVideo("I9we9cTyyck", "Voltamos! | Age of Empires 2"),
            YoutubeVideo("rThXHQ3G7jE", "SHOWMATCH DE TITÃS - TEAM GABI x TEAM BK - Jogo 1 | Age of Empires 2"),
            YoutubeVideo("O0-raaw0VT8", "REVIVENDO MOMENTOS HISTÓRICOS - TEAM GABI x TEAM BK - Jogo 3 | Age of Empires 2"),
            YoutubeVideo("AkPOZDN9PJs", "Viciados em Nômades! Bk x Ss | Age of Empires 2"),
            YoutubeVideo("fgHBsYI6XWY", "Aí é duro | Age of Empires 2"),
            YoutubeVideo("3uQCPVj24C4", "UM BATE E VOLTA DE GIGANTES - ChampionSheep - LIGA TITÃ (bruh x Rodrixxxs) Jogo 1 | Age of Empires 2"),
            YoutubeVideo("HglVpNUq6nc", "UM JOGO MARCANTE - Brasil x Argentina - Jogo 4 | Age of Empires 2"),
            YoutubeVideo("SgTg2tMIik8", "A GRANDE GUERRA DA FORTALEZA - Pater x Felino Molhado - Jogo 3 | Age of Empires 2"),
            YoutubeVideo("UpU0ABSanjc", "JOGADORES BRUTOS! - ChampionSheep - LIGA TITÃ (Pedreiro x Alive) Jogo 1 | Age of Empires 2"),
            YoutubeVideo("Yhv8YV3-_qU", "Não entendi | Age of Empires 2"),
            YoutubeVideo("dqMIWw-wlfI", "PERDEMOS 4 SEGUIDAS ANTES DESSA | Age of Empires 2"),
            YoutubeVideo("TvF-Gb8dDwA", "GRANDES TOMADAS DE DECISÃO - ChampionSheep - LIGA TITÃ (Jubileu x Lucho) Jogo 1 | Age of Empires 2"),
            YoutubeVideo("MXDqPE54ZBA", "CHAMADAS NIVEL BIOGRAFIA DE TINDER | Age of Empires 2"),
            YoutubeVideo("ttSBRPb8eU0", "PERRENGUE NA ARÁBIA | Age of Empires 2"),
            YoutubeVideo("FyVW_y1ygsA", "SEUS CERVOS? NOSSOS CERVOS! | Age of Empires 2"),
            YoutubeVideo("5Pdkg-iM4zc", "Contra-ataque do contra-ataque! Bk x Ss | Age of Empires 2"),
            YoutubeVideo("QQHMCWizdh4", "MUITO VIL, POUCO GAME | Age of Empires 2"),
            YoutubeVideo("DAoXOtalCQk", "ESSE JOGO FOI INSANO - ChampionSheep - LIGA TITÃ (Dogão x st4rk) Jogo 2 | Age of Empires 2"),
            YoutubeVideo("t9h5Q-bpowc", "PERRENGUE NO LAGO FANTASMA | Age of Empires 2"),
            YoutubeVideo("kQmDnPYu8O0", "TODOS NA MAIS PURA CALMARIA | Age of Empires 2"),
            YoutubeVideo("mAiCntnZlLs", "ESSE MAPA É SÓ PANCADARIA! - DOGÃO x MIGUEL - Jogo 4 | Age of Empires 2"),
            YoutubeVideo("Z7UBQeGlbcc", "DUELO ENTRE CARECA E CALVO - ChampionSheep - LIGA TITÃ (Dogão x Capoch) Jogo 1 | Age of Empires 2"),
            YoutubeVideo("zD4YK7KBJBg", "14? | Age of Empires 2"),
            YoutubeVideo("81nZ-VTmmDY", "TENTOU, MAS NÃO ROLOU - ChampionSheep - LIGA TITÃ (Sebastian x Nicov) Jogo 7 | Age of Empires 2"),
            YoutubeVideo("_TPCNtx9LXs", "UMA BATALHA SUL-ASIÁTICA - ChampionSheep - LIGA TITÃ (Ny4jin X Monoz_zZ) Jogo 2 | Age of Empires 2"),
            YoutubeVideo("v_DLxDTclaA", "ESTILO TROPA DE ELITE - TEAM GABI x TEAM BK - Jogo 4 | Age of Empires 2"),
            YoutubeVideo("Z68cdGkMl7E", "A MICRO TA EM DIA - ChampionSheep - LIGA TITÃ (st4rk x Dream) Jogo 2 | Age of Empires 2"),
            YoutubeVideo("w9rHAgovUQ0", "QUE COMEÇO... DIFERENTE... - ChampionSheep - LIGA TITÃ (Dogão x Capoch) Jogo 3 | Age of Empires 2"),
            YoutubeVideo("XP89ux6Jqe8", "HERMANOS (parte 2) | Age of Empires 2"),
            YoutubeVideo("mtx1evMr_PY", "O FILHO DO CAOS - ChampionSheep - LIGA OURO (Klecius x El_Ti_Rex) Jogo 3 | Age of Empires 2"),
            YoutubeVideo("Q4_1NNXd9A4", "QUE FINAL EM... - ChampionSheep - LIGA OURO (Clark x Snapy) Jogo 4 | Age of Empires 2"),
            YoutubeVideo("DMw4GrnJVCs", "UM ERRO FATAL! - ChampionSheep - LIGA TITÃ (bruh x Rodrixxxs) Jogo 2 | Age of Empires 2"),
            YoutubeVideo("xUCYYT2VCh4", "OS FANTASMAS DO LAGO - Brasil x Argentina - Jogo 3 | Age of Empires 2"),
            YoutubeVideo("KDAxa_zrLnE", "NIVEL DE JOGO ALTO - ChampionSheep - LIGA TITÃ (bruh x Nicov) Jogo 3 | Age of Empires 2"),
            YoutubeVideo("GE4Ogt3W64Q", "O CUSTO DA MICRAGEM - ChampionSheep - LIGA TITÃ (Ny4jin X Monoz_zZ) Jogo 3 | Age of Empires 2"),
            YoutubeVideo("KAFPE1_Dgkc", "AS SEMI-FINAIS - ChampionSheep - LIGA TITÃ (Sebastian x Nicov) Jogo 1 | Age of Empires 2"),
            YoutubeVideo("uuVBvu__4pM", "ESSA CIV É MUITO COMPLETA - ChampionSheep - LIGA OURO (Clark x Snapy) Jogo 3 | Age of Empires 2"),
            YoutubeVideo("4h2mVpA8u30", "AONDE AS LENDAS SÃO FORJADAS - ChampionSheep - LIGA TITÃ (Dogão x Nicov) Jogo 2 | Age of Empires 2"),
            YoutubeVideo("tt7O_XS6I5Y", "A ARÁBIA DE LEI - ChampionSheep - LIGA TITÃ (Dogão x st4rk) Jogo 3 | Age of Empires 2"),
            YoutubeVideo("ZjlWDGyfbCk", "TENTOU FAZER UMA MALDADE - ChampionSheep - LIGA TITÃ (Mehmed x chaelzzz) Jogo 1 | Age of Empires 2"),
            YoutubeVideo("FFd4RMVpz9M", "ALEXANDRE FOI GRANDE MESMO? - ChampionSheep - LIGA TITÃ (st4rk x Dream) Jogo 1 | Age of Empires 2"),
            YoutubeVideo("2jsM-TVJzbA", "SPEEDRUN DE AGE OF EMPIRES - ChampionSheep - LIGA TITÃ (chaelzzz x Pituh) Jogo 2 | Age of Empires 2"),
            YoutubeVideo("JsvLpY8I3j4", "O TAL DO PAUSADINHAS | Age of Empires 2"),
            YoutubeVideo("9PIPFUZ2DUE", "BRASIL CONTRA BRASIL - ChampionSheep - LIGA TITÃ (Dogão x st4rk) Jogo 1 | Age of Empires 2"),
            YoutubeVideo("p5EL0UtFnt4", "O caminho longo - Pater x Felino Molhado - Jogo 4 | Age of Empires 2"),
            YoutubeVideo("4A5Wnm2lDDY", "O PODER DO DESCONTO - ChampionSheep - LIGA TITÃ (Sebastian x miguel) Jogo 2 | Age of Empires 2"),
            YoutubeVideo("m8c9IfOs4OM", "FINAL DA LIGA OURO - ChampionSheep - LIGA OURO (Clark x Snapy) Jogo 1 | Age of Empires 2"),
            YoutubeVideo("wO6PL3Tuaeo", "BAZINGA! | Age of Empires 2"),
            YoutubeVideo("MdRifhUYHP8", "O MAIS ALTO NIVEL DO AGE - ChampionSheep - LIGA TITÃ (Sebastian x miguel) Jogo 1 | Age of Empires 2"),
            YoutubeVideo("QaWmkzETwsQ", "O DUELO FS - ChampionSheep - LIGA PRATA (N. Mendes x Hitshangui) Jogo 1 | Age of Empires 2"),
            YoutubeVideo("FQkKH08RTE8", "APENAS ENCANADORES ONLINE - ChampionSheep - LIGA TITÃ (Sebastian x Nicov) Jogo 2 | Age of Empires 2"),
            YoutubeVideo("WtjIrgqLZIY", "INAUGURANDO A LIGA PRATA - ChampionSheep - LIGA PRATA (Weslan x Xefres) Jogo 1 | Age of Empires 2"),
            YoutubeVideo("IHAR_Fwn4Bw", "LOUCURA DO COMEÇO AO FIM - ChampionSheep - LIGA OURO (Klecius x El_Ti_Rex) Jogo 2 | Age of Empires 2"),
            YoutubeVideo("sn18REpnA34", "ELE VEIO ASSISTIR A PARTIDA - ChampionSheep - LIGA OURO (Alone x Gen. Enk) Jogo 2 | Age of Empires 2"),
            YoutubeVideo("MDvfwaplyPc", "INESQUECÍVEL ESSE COMEÇO - ChampionSheep - LIGA BRONZE (Powerstone x Foka) Jogo 2 | Age of Empires 2"),
            YoutubeVideo("k4Rqa6xW4to", "RESPEITE OS CAMELOS - ChampionSheep - LIGA OURO (Alone x Gen. Enk) Jogo 3 | Age of Empires 2"),
            YoutubeVideo("rQF8QjQ0Qps", "A GRANDE FINAL!! - ChampionSheep - LIGA TITÃ (Dogão x Nicov) Jogo 1 | Age of Empires 2"),
            YoutubeVideo("PihlP6aNFck", "ANTICLIMÁTICO... - ChampionSheep - LIGA OURO (Alone x Gen. Enk) Jogo 4 | Age of Empires 2"),
            YoutubeVideo("5gd2Yw8mp2o", "DE VEZ EM QUANDO PODE - ChampionSheep - LIGA TITÃ (bruh x Nicov) Jogo 1 | Age of Empires 2"),
            YoutubeVideo("R8h272hyBA4", "O ZORRO ERA? | Age of Empires 2"),
            YoutubeVideo("uU-KqGf5-Js", "O FOLLEN VIROU SOMMELIER | Age of Empires 2"),
            YoutubeVideo("KrWlqfOpd14", "SEBASTIAN A LENDA - ChampionSheep - LIGA TITÃ (Sebastian x Nicov) Jogo 4 | Age of Empires 2"),
            YoutubeVideo("fo9o4vRkJtI", "APENAS CONVERSAS FAMILY FRIENDLY! | Age of Empires 2"),
            YoutubeVideo("q1w1l6gD9Lk", "ELE NÃO CONHECE O PRE-BOAR | Age of Empires 2"),
            YoutubeVideo("UBJy7KgAgcY", "ELE DEU UMA BRINCADA - ChampionSheep - LIGA TITÃ (chaelzzz x Pituh) Jogo 1 | Age of Empires 2"),
            YoutubeVideo("yzeex8dD6As", "ODIADOR DE DRAGÕES - ChampionSheep - LIGA TITÃ (Dogão x Capoch) Jogo 2 | Age of Empires 2"),
            YoutubeVideo("9ISKUsN5q4s", "O AMOR AO AGE FOI MAIOR - ChampionSheep - LIGA BRONZE (Gomes x Onfire) Jogo 1 | Age of Empires 2"),
            YoutubeVideo("i6v-I86nVgM", "ÉPOCA DE FLIPERAMA - ChampionSheep - LIGA BRONZE (Gomes x Onfire) Jogo 2 | Age of Empires 2"),
            YoutubeVideo("D3Rb-oN1Ho8", "FOI UM ESPANCO - ChampionSheep - LIGA TITÃ (Sebastian x miguel) Jogo 3 | Age of Empires 2"),
            YoutubeVideo("lqV0ccQ1dzI", "UM JOGO DE DORES - ChampionSheep - LIGA PRATA (N. Mendes x Hitshangui) Jogo 2 | Age of Empires 2"),
            YoutubeVideo("6VC9c9SVMJA", "A BALSA DO GG - ChampionSheep - LIGA TITÃ (Sebastian x Nicov) Jogo 3 | Age of Empires 2"),
            YoutubeVideo("OJc7jONlpR0", "ESSE MAPA É 8 OU 80 - ChampionSheep - LIGA TITÃ (Nahue x Dogão) Jogo 2 | Age of Empires 2"),
            YoutubeVideo("eJU2nLnIt3w", "O gigante brasileiro: F1RE! | Age of Empires 2"),
            YoutubeVideo("FnX168QMJhg", "JOGANDO NA FORÇA DO ÓDIO | Age of Empires 2"),
            YoutubeVideo("VOfISU8LX10", "A RESENHA TOMOU CONTA - ChampionSheep - LIGA OURO (Klecius x El_Ti_Rex) Jogo 4 | Age of Empires 2"),
            YoutubeVideo("wUyY6sboPhg", "JOGADORES CAROS - ChampionSheep - LIGA TITÃ (Ny4jin X Monoz_zZ) Jogo 1 | Age of Empires 2"),
            YoutubeVideo("a-n-BdR2s0w", "SE NÃO GOSTOU... | Age of Empires 2"),
            YoutubeVideo("vKxvmboRvQY", "A IDADE ESTÁ CHEGANDO - ChampionSheep - LIGA TITÃ (bruh x Nicov) Jogo 2 | Age of Empires 2"),
            YoutubeVideo("BprcY-KMyqg", "O CARA MICRA MUITO! | Age of Empires 2"),
            YoutubeVideo("fgOH9Eev0DA", "O MELHOR FILME DA DISNEY | Age of Empires 2"),
            YoutubeVideo("Tw4nnhaEZt8", "EU COMETI UM ERRO... - ChampionSheep - LIGA BRONZE (Powerstone x Foka) Jogo 3? | Age of Empires 2"),
            YoutubeVideo("TVdGStK2-ME", "CABEÇA DINOSSAURO - ChampionSheep - LIGA OURO (Klecius x El_Ti_Rex) Jogo 1 | Age of Empires 2"),
            YoutubeVideo("QpaSo-nI93Q", "ESTÃO QUERENDO NOS SABOTAR - ChampionSheep - LIGA OURO (Clark x Snapy) Jogo 2 | Age of Empires 2"),
            YoutubeVideo("5s7orRizKjc", "A GRANDE LIGA BRONZE - ChampionSheep - LIGA BRONZE (Powerstone x Foka) Jogo 1 | Age of Empires 2"),
            YoutubeVideo("ChTgIz49XUU", "O TAL DO INCAS POCKET | Age of Empires 2"),
            YoutubeVideo("CzmbLsJwKRY", "SAIU DO BAN E JÁ TA ASSIM | Age of Empires 2"),
            YoutubeVideo("pPF_XTeZrPo", "O GRANDE CAVALEIRO AZUL - ChampionSheep - LIGA BRONZE (Gomes x Onfire) Jogo 3 | Age of Empires 2"),
            YoutubeVideo("zQ6r_KqaRk0", "FELIZ ANIVERSÁRIO POLIDORO | Age of Empires 2"),
            YoutubeVideo("XWYh0ZbDaks", "O RUSH DE ARQUEIROS TAMBÉM DOMINA AQUI | Age of Empires 3"),
            YoutubeVideo("QDVl0-TXuKg", "A DAMA DE FERRO - ChampionSheep - LIGA TITÃ (Sebastian x Nicov) Jogo 6 | Age of Empires 2"),
            YoutubeVideo("k48uiXg1NHw", "BORA BORA MOCHILEIROS - Pater x Felino Molhado - Jogo 1 | Age of Empires 2"),
            YoutubeVideo("W8IVggWqyBk", "CORRIGINDO O ERRO - ChampionSheep - LIGA BRONZE (Powerstone x Foka) Jogo 4 | Age of Empires 2"),
            YoutubeVideo("rrkN3dY2ltk", "O CARA TA LAGADO | Age of Empires 2"),
            YoutubeVideo("tPTLMhsvNEk", "A MAIOR LIVE DE AGE 3 DO MUNDO | Age of Empires 3"),
            YoutubeVideo("pwaWHM14O0w", "A CIV DO CARA É MUITO FORTE | Age of Empires 3"),
            YoutubeVideo("ZeuLrBJjR2g", "TOMADOS PELO ESPÍRITO DA BOBICE | Age of Empires 3"),
            YoutubeVideo("xVaokBROlDs", "O SOUND DESIGN DESSE JOGO É IMPECÁVEL | Age of Empires 3"),
            YoutubeVideo("RudSZ5SQHRU", "JOGANDO NO MODO HARD | Age of Empires 3"),
            YoutubeVideo("cgD2LjM8vdE", "O CARA TAVA PERDIDO | Age of Empires 3"),
            ]

    def get_random_video(self):
        return random.choice(self._videos)


class DiscordMessenger:

    def __init__(self, logger, access_token, public_channel, maintenance_channel):
        self._logger = logger
        self._public_channel = public_channel
        self._maintenance_channel = maintenance_channel
        self._access_token = access_token

    def _send_message(self, channel, message):
        response = requests.post(
            f"https://discordapp.com/api/channels/{channel}/messages",
            headers={"Authorization": f"Bot {self._access_token}"},
            json={"content": message}
        )
        response.raise_for_status()
        json_data = response.json()
        return json_data['id']

    def send_general_message(self, message):
        self._logger.debug("Sending message to general channel on Discord: {}".format(message))
        self._send_message(self._public_channel, message)

    def send_maintenance_message(self, message):
        self._logger.debug("Sending message to maintenance channel on Discord: {}".format(message))
        self._send_message(self._maintenance_channel, message)


class SharkatzorConfiguration:

    def __init__(self, logger, config_file_path):
        self._logger = logger
        self._config = self._read_config(config_file_path)

    def _read_config(self, config_file_path):
        config = configparser.ConfigParser()
        config.read(config_file_path)
        return config

    def _log_configuration(self):
        self._logger.info(f'Youtube channel ID: {self._config["youtube"]["channel_id"]}')
        self._logger.info('Discord Token: {}****'.format(self._config["discord"]["token"][:4]))
        self._logger.info('General Discord channel: {}****'.format(self._config["discord"]["general_channel"][:4]))
        self._logger.info('Maintenance Discord channel: {}****'.format(self._config["discord"]["maintenance_channel"][:4]))
        self._logger.info('Youtube keys (number): {}'.format(len(self._config["youtube"]["keys"].split(','))))

    @property
    def configuration(self):
        return self._config


class SharkatzorYoutubeVacation:

    def __init__(self, config_path):
        self._logger = LOGGER
        self._logger.info('Starting ...')
        self._config = self._load_configuration(config_path)

    def _load_configuration(self, config_path):
        shark_config = SharkatzorConfiguration(self._logger, config_path)
        return shark_config.configuration

    def run(self):
        random_video = RandomVideo().get_random_video()
        discord = DiscordMessenger(self._logger, self._config["discord"]["token"], self._config["discord"]["general_channel"], self._config["discord"]["maintenance_channel"])
        video_url = f"https://www.youtube.com/watch?v={random_video.id}"
        try:
            discord.send_general_message(f"🌴 Durante as merecidas férias do Tomahawk, aproveite para conferir um vídeo com os 100 melhores visualizações.\n**{random_video.title}**\n{video_url}")
        except Exception as err:
            self._logger.error(f"Could not send message to Discord: {err}")
            discord.send_maintenance_message(f"Caramba, não consegui mandar mensagem no Discord!:\n{err}")


def main():
    argparser = argparse.ArgumentParser(description="Sharkatzor Youtube Notifier for Discord")
    argparser.add_argument("-c", "--config", help="Configuration file path", default="/etc/sharkatzor/conf.ini")
    args = argparser.parse_args()

    sharkatzor = SharkatzorYoutubeVacation(args.config)
    sharkatzor.run()


if __name__ == "__main__":
    main()
