import googleapiclient.discovery
from googleapiclient.errors import HttpError
import logging


LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)
S_HANDLER = logging.StreamHandler()
S_HANDLER.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s [%(levelname)s] %(message)s')
S_HANDLER.setFormatter(formatter)
LOGGER.addHandler(S_HANDLER)


class YoutubeVideo(object):
    def __init__(self, id="", title=""):
        self.id = id
        self.title = title

    def __eq__(self, other):
        return self.id == other.id

    @property
    def link(self):
        return f"https://www.youtube.com/watch?v={self.id}"

    @staticmethod
    def generate(video_data: dict):
        return YoutubeVideo(video_data["id"], video_data["snippet"]["title"])


class YoutubeScrapper:

    def __init__(self, logger, gcp_api_keys):
        self._logger = logger
        self._youtube = None
        self._gcp_api_keys = gcp_api_keys
        self._playlist = None

    def _login_youtube(self, channel_id):
        self._logger.debug("Executing YT login")
        for key in self._gcp_api_keys:
            try:
                self._logger.info("Connecting to YT with key {}****".format(key[:8]))
                self._youtube = googleapiclient.discovery.build("youtube", "v3", developerKey=key)
                request = self._youtube.channels().list(part="id,contentDetails", id=channel_id, maxResults=1)
                response = request.execute()
                if not response:
                    message = "Could not login on Youtube!"
                    self._logger.error(message)
                self._playlist = response["items"][0]["contentDetails"]["relatedPlaylists"]["uploads"]
                self._logger.info(f"Logged-in on youtube, Playlist ID: {self._playlist}")
                return
            except HttpError as err:
                self._logger.error(err.reason)
                pass
        raise Exception("Could not login on YT! Giving up!")

    def get_most_watched_videos(self, channel_id: str, max_results=200) -> list:
        if self._youtube is None:
            self._login_youtube(channel_id)
        playlist_items = []
        next_page_token = None

        while len(playlist_items) < max_results:
            playlist_request = self._youtube.playlistItems().list(
                part="contentDetails",
                playlistId=self._playlist,
                maxResults=50,
                pageToken=next_page_token,
            )
            playlist_response = playlist_request.execute()
            playlist_items.extend(playlist_response["items"])
            next_page_token = playlist_response.get("nextPageToken")
            if not next_page_token:
                break

        video_ids = [item["contentDetails"]["videoId"] for item in playlist_items]

        videos = []
        # INFO: Youtube API allows only 50 videos per request
        for i in range(0, len(video_ids), 50):
            video_ids_chunk = video_ids[i:i + 50]
            videos_request = self._youtube.videos().list(
                part="snippet,contentDetails,statistics",
                id=",".join(video_ids_chunk),
            )
            videos_response = videos_request.execute()
            videos.extend(videos_response.get("items", []))

        videos.sort(key=lambda x: int(x["statistics"]["viewCount"]), reverse=True)
        result = [YoutubeVideo.generate(it) for it in videos]
        return result


if __name__ == "__main__":
    api_key = ["????", "????", "????", "????"]
    # https://www.youtube.com/channel/UCJ0vp6VTn7JuFNEMj5YIRcQ: @Tomahawkaoe
    channel_id = "UCJ0vp6VTn7JuFNEMj5YIRcQ"

    scrapper = YoutubeScrapper(LOGGER, api_key)
    videos = scrapper.get_most_watched_videos(channel_id)

    for video in videos:
        LOGGER.info(f"[{video.id}] {video.title}")
